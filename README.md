# NagiosVelvice - Nagios Velvice Alert Panel

## Description

![Velvice](./doc/velvice.png)
Nagios VELVICE is an acronym for "Nagios leVEL serVICE status".

The Nagios web page is sometimes very graphically charged and does not necessarily contain the information you need at a glance.
For example, it is quite complicated to restart controls on multiple hosts in one click.

For example, a server that is down should take only one line and not one per service...
Similarly, a service that has been down for 5 minutes or since yesterday has more weight than a service that has fallen for 15 days.

 * With Velvice Panel, a broken down server takes only one line.
   Services that have been falling for a long time gradually lose their color and become pastel colors.

 * With Velvice Panel, it is possible through a single click to redo a check of all services that are in the CRITICAL state.
   Similarly, it is possible to restart a check on all SSH services in breakdowns...
   In order not to clog the Nagios server, checks are shifted by 2 seconds in time.

There is also a link to the web page of the main Nagios server.
For each computer, you have a direct link to its dedicated web page on this server.

The tool is quite effective and tries to keep things simple
but easily configurable for your site like a swiss army knife.
Everything is configured in a YAML file.

All the command help and description is on the online manual
[velvice](https://legi.gricad-pages.univ-grenoble-alpes.fr/soft/trokata/nagios-velvice).


## Debian package

Debian is a GNU/Linux distribution.
Debian (and certainly Ubuntu) package for amd64 arch could be download on: https://legi.gricad-pages.univ-grenoble-alpes.fr/soft/trokata/nagios-velvice/download/.

You can then install it with

```bash
sudo dpkg -i nagios3-velvice*_amd64.deb
```
(just replace * with the version you have donwloaded).


## Software repository

All code is under **free license**.
Scripts in `bash` are under GPL version 3 or later (http://www.gnu.org/licenses/gpl.html),
the `perl` scripts are under the same license as `perl` itself ie the double license GPL and Artistic License (http://dev.perl.org/licenses/artistic.html).

All sources are available on the campus forge: https://gricad-gitlab.univ-grenoble-alpes.fr/legi/soft/trokata/nagios-velvice

The sources are managed via subversion (http://subversion.tigris.org/).
It is very easy to stay synchronized with these sources

 * initial recovery
```bash
git clone https://gricad-gitlab.univ-grenoble-alpes.fr/legi/soft/trokata/nagios-velvice
```
 * the updates thereafter
```bash
git pull
```

It is possible to have access to writing at the forge on reasoned request to [Gabriel Moreau](mailto:Gabriel.Moreau__AT__legi.grenoble-inp.fr).
For issues of administration time and security, the forge is not writable without permission.
For the issues of decentralization of the web, autonomy and non-allegiance to the ambient (and North American) centralism, we use our own forge...

You can propose an email patch of a particular file via the `diff` command.
Note that `svn` defaults to the unified format (`-u`).
Two examples:
```bash
diff -u nagios-velvice.org nagios-velvice.new > nagios-velvice.patch
svn diff nagios-velvice > nagios-velvice.patch
```
We apply the patch (after having read and read it again) via the command
```bash
patch -p0 < nagios-velvice.patch
```
