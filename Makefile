SHELL:=/bin/bash

DESTDIR=

CGIDIR=/usr/lib/cgi-bin/nagios3
ETCDIR=/etc/nagios3
MANDIR=/usr/share/man/man1
SHAREDOCDIR=/usr/share/doc/nagios-velvice
STYLEDIR=/etc/nagios3/stylesheets
JSDIR=/usr/share/nagios3/htdocs/js

.PHONY: all ignore install update sync upload stat help pkg pages

all:
	pod2man velvice.cgi | gzip > velvice.1.gz
	pod2html --css podstyle.css --index --header velvice.cgi > velvice.html

install: update

update:
	@install -d -m 0755 -o root -g root $(DESTDIR)/$(CGIDIR)
	@install -d -m 0755 -o root -g root $(DESTDIR)/$(ETCDIR)
	@install -d -m 0755 -o root -g root $(DESTDIR)/$(MANDIR)
	@install -d -m 0755 -o root -g root $(DESTDIR)/$(SHAREDOCDIR)
	@install -d -m 0755 -o root -g root $(DESTDIR)/$(STYLEDIR)
	@install -d -m 0755 -o root -g root $(DESTDIR)/$(JSDIR)

	install    -m 0755 -o root -g root velvice.cgi             $(DESTDIR)/$(CGIDIR)
	install    -m 0644 -o root -g root velvice.sample.yml      $(DESTDIR)/$(ETCDIR)
	install    -m 0644 -o root -g root velvice.1.gz            $(DESTDIR)/$(MANDIR)
	install    -m 0644 -o root -g root LICENSE.txt             $(DESTDIR)/$(SHAREDOCDIR)
	install    -m 0644 -o root -g root velvice.html            $(DESTDIR)/$(SHAREDOCDIR)
	install    -m 0644 -o root -g root velvice.sample.yml      $(DESTDIR)/$(SHAREDOCDIR)
	install    -m 0644 -o root -g root velvice.css             $(DESTDIR)/$(STYLEDIR)
	install    -m 0644 -o root -g root velvice-theme-light.css $(DESTDIR)/$(STYLEDIR)
	install    -m 0644 -o root -g root velvice-theme-dark.css  $(DESTDIR)/$(STYLEDIR)
	install    -m 0644 -o root -g root velvice.js              $(DESTDIR)/$(JSDIR)

sync:
	svn update

upload:
	cadaver --rcfile=cadaverrc

pkg: all
	./make-package-debian

pages: all pkg
	mkdir -p public/download
	cp -p *.html             public/
	cp -p podstyle.css       public/
	cp -p LICENSE.txt        public/
	cp -p velvice.sample.yml public/
	cp -p --no-clobber nagios3-velvice_*_all.deb  public/download/
	cd public; ln -sf velvice.html index.html
	echo '<html><body><h1>Nagios-Velvice Debian Package</h1><ul>' > public/download/index.html
	(cd public/download; while read file; do printf '<li><a href="%s">%s</a> (%s)</li>\n' $$file $$file $$(stat -c %y $$file | cut -f 1 -d ' '); done < <(ls -1t *.deb) >> index.html)
	echo '</ul></body></html>' >> public/download/index.html

stat:
	svn log|egrep '^r[[:digit:]]'|egrep -v '^r1[[:space:]]'|awk '{print $$3}'|sort|uniq -c                 |gnuplot -p -e 'set style fill solid 1.00 border 0; set style histogram; set style data histogram; set xtics rotate by 0; set style line 7 linetype 0 linecolor rgb "#222222"; set grid ytics linestyle 7; set xlabel "User contributor" font "bold"; set ylabel "Number of commit" font "bold"; plot "/dev/stdin" using 1:xticlabels(2) title "commit" linecolor rgb "#666666"'
	(echo '0 2015'; svn log|egrep '^r[[:digit:]]'|awk '{print $$5}'|cut -f 1 -d '-'|sort|uniq -c)|sort -k 2|gnuplot -p -e 'set style fill solid 1.00 border 0; set style histogram; set style data histogram; set xtics rotate by 0; set style line 7 linetype 0 linecolor rgb "#222222"; set grid ytics linestyle 7; set xlabel "Year"             font "bold"; set ylabel "Number of commit" font "bold"; plot "/dev/stdin" using 1:xticlabels(2) title "commit" linecolor rgb "#666666"'

help:
	@echo "Possibles targets:"
	@echo " * all     : make manual"
	@echo " * install : complete install"
	@echo " * update  : update install (do not update cron file)"
	@echo " * sync    : sync with official repository"
	@echo " * upload  : upload on public dav forge space"
	@echo " * stat    : svn stat with gnuplot graph"
	@echo " * pkg     : build Debian package"
	@echo " * ignore  : svn rules to ignore some files"

ignore: svnignore.txt
	svn propset svn:ignore -F svnignore.txt .
	svn propset svn:keywords "Id" velvice.cgi
