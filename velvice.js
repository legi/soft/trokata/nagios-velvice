// velvice.js
// Copyright (C) 2014-2024, LEGI UMR 5519 / CNRS UGA G-INP, Grenoble, France
// Written by Gabriel Moreau <Gabriel.Moreau(A)univ-grenoble-alpes.fr>, LEGI UMR 5519, CNRS, Grenoble - France
// Licence GNU GPL version 2 or later and Perl equivalent

var refresh_sec = 900; // 15 min = 900 s
var refresh_interval;

function refresh() {
   var req = new XMLHttpRequest();
   var datenow = new Date();
   cgi_script_name = document.getElementById('refresh').getAttribute('href');
   console.log('Grabbing Value:', cgi_script_name);
   req.open('GET', cgi_script_name.concat('?only=body'), true); // Grabs whatever you've written in this file
   req.onload = function () {
      if (req.status == 200) {
         document.getElementById('master-body').innerHTML = req.responseXML.getElementById('master-body').innerHTML;
         console.log('Update Page at:', datenow.toLocaleTimeString());

         refresh_next = req.responseXML.getElementById('master-body').getAttribute('refresh');
         if (refresh_next < 60 && refresh_next != 0) {
            refresh_next = 60;
            }
         if (refresh_next != refresh_sec) {
            refresh_sec = refresh_next;
            clearInterval(refresh_interval);
            refresh_interval = setInterval(refresh, refresh_sec * 1000);
            console.log('Update at:', datenow.toLocaleTimeString(), 'Refresh Interval:', refresh_sec);
            document.getElementById('master-body').setAttribute('refresh', refresh_sec);
            }
         }
      }
   req.send(null);
   }

function init() { // This is the function the browser first runs when it's loaded.
   refresh_sec = document.getElementById('master-body').getAttribute('refresh');
   if (refresh_sec < 60 && refresh_sec != 0) {
      refresh_sec = 60;
      }
   // Set the refresh() function to run every 900 seconds. 1 second would be 1000
   refresh_interval = setInterval(refresh, refresh_sec * 1000);
   console.log('Start Refresh Interval:', refresh_sec);
   }
